using System.Collections.Generic;

namespace Lairway{
  public class ThreatState{
    
    private Piece target;
    
    private HashSet<Piece> aids = new HashSet<Piece>();
    private HashSet<Piece> threats = new HashSet<Piece>();
    
    
    public ThreatState(Piece target){
      this.target = target;
    }
    
    
    public bool AddArrow(Arrow arrow){
      if(arrow.Target != target){
        //Todo: log error
        return false;
      }
      
      if(arrow.IsFrendly()){
        aids.Add(arrow.Source);
      }else{
        threats.Add(arrow.Source);
      }
      
      return true;
    }
    
    
    public ThreatLevel GetThreat(){
      if(threats.Count < 1){
        return ThreatLevel.Normal;
      }
    
      int trueThreat = threats.Count - aids.Count;
      
      if(trueThreat < 1){
        return ThreatLevel.WinningChain;
      }
      
      return ThreatLevel.LosingChain;
    }
    
    
    public Piece Target{
      get{
        return target;
      }
    }
    
  }
}
