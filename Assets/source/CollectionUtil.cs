using System.Collections.Generic;

namespace Lairway{
  public class CollectionUtil<T>{
    
    
    public static bool AddIfNotNull(ICollection<T> collection, T item){
      if (item == null){
        return false;
      }
      
      collection.Add(item);
      return true;
    }
    
    
    public static HashSet<T> AsHashSet(List<T> list){
      HashSet<T> hashSet = new HashSet<T>();
      
      foreach(T item in list){
        hashSet.Add(item);
      }
      
      return hashSet;
    }
    
    
  }
}