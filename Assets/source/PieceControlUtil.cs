using System.Collections.Generic;

namespace Lairway{
  public class PieceControlUtil{
    
    
    public static HashSet<Square> GetControlledSquares(Piece piece, BattleStage battleStage){
      return GetControlledSquares(piece, battleStage, null);
    }
    
    
    public static HashSet<Square> GetControlledSquares(Piece piece, BattleStage battleStage, Piece ignoredPiece){
      return piece.Stance.GetControlledSquares(piece.Position, battleStage, ignoredPiece);
    }
    
    
    public static HashSet<Square> GetControlledSquares(HashSet<Piece> pieces, BattleStage battleStage){
      return GetControlledSquares(pieces, battleStage, null);
    }
    
    
    public static HashSet<Square> GetControlledSquares(HashSet<Piece> pieces, BattleStage battleStage, Piece ignoredPiece){
      HashSet<Square> controlled = new HashSet<Square>();
      
      foreach(Piece piece in pieces){
        controlled.UnionWith(GetControlledSquares(piece, battleStage, ignoredPiece));
      }
      
      return controlled;
    }
    
    
  }
}