namespace Lairway{
  public class Arrow{
    
    private Piece source;
    private Piece target;
    
    
    public Arrow(Piece source, Piece target){
      this.source = source;
      this.target = target;
    }
    
    
    // maybe this should be an enum instead of boolean?
    public bool IsFrendly(){
      return source.Band == target.Band;
    }
    
    
    public Piece Source{
      get{
        return source;
      }
    }
    
    
    public Piece Target{
      get{
        return target;
      }
    }
    
  }
}