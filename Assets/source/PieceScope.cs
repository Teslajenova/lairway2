using System.Collections.Generic;

namespace Lairway{
  public class PieceScope{
    
    private Piece piece;
    private BattleStage battleStage;
    
    private HashSet<Square> moveRange;
    private HashSet<Piece> attackRange; 
    
    private Dictionary<Square, HashSet<Piece>> coveringAllies = new Dictionary<Square, HashSet<Piece>>();
    private Dictionary<Square, HashSet<Piece>> threateningEnemies = new Dictionary<Square, HashSet<Piece>>();
    
    private Dictionary<Square, HashSet<Piece>> coveredAllies = new Dictionary<Square, HashSet<Piece>>();
    private Dictionary<Square, HashSet<Piece>> threatenedEnemies = new Dictionary<Square, HashSet<Piece>>();
    
    
    public PieceScope(Piece piece, BattleStage battleStage, bool diagonal){
      this.piece = piece;
      this.battleStage = battleStage;
      
      moveRange = PieceMoverUtil.GetMoveRange(piece, battleStage, diagonal); 
      
      SetStaticInfluences();
      
      SwitchStance(piece.Stance);
      
      attackRange = PieceCoverUtil.GetCoveredPieces(piece.Position, piece.Stance, piece.Band.Opposite(), battleStage);
    }
    
    
    private void SetStaticInfluences(){
      foreach(Square square in moveRange){
        coveringAllies.Add(square, PieceCoverUtil.GetCoveringPieces(square, piece.Band, battleStage, piece));
        
        threateningEnemies.Add(square, PieceCoverUtil.GetCoveringPieces(square, piece.Band.Opposite(), battleStage, piece));
      }
    }
    
    
    public void SwitchStance(Stance stance){
      coveredAllies.Clear();
      threatenedEnemies.Clear();
      
      foreach(Square square in moveRange){
        coveredAllies.Add(square, PieceCoverUtil.GetCoveredPieces(square, stance, piece.Band, battleStage, piece));
        
        threatenedEnemies.Add(square, PieceCoverUtil.GetCoveredPieces(square, stance, piece.Band.Opposite(), battleStage, piece));
      }
    }
    
    
    public Piece Piece{
      get{
        return piece;
      }
    }
    
    
    public HashSet<Square> MoveRange{
      get{
        return new HashSet<Square>(moveRange);
      }
    }
    
    
    public HashSet<Piece> AttackRange{
      get{
        return new HashSet<Piece>(attackRange);
      }
    }
    
    
    public HashSet<Piece> GetCoveringAllies(Square square){
      if(!coveringAllies.ContainsKey(square)) {
        return new HashSet<Piece>();
      }

      return new HashSet<Piece>(coveringAllies[square]);
    }
    
    
    public HashSet<Piece> GetCoveredAllies(Square square){
      if(!coveredAllies.ContainsKey(square)) {
        return new HashSet<Piece>();
      }

      return new HashSet<Piece>(coveredAllies[square]);
    }
    
    
    public HashSet<Piece> GetThreateningEnemies(Square square){
      if(!threateningEnemies.ContainsKey(square)) {
        return new HashSet<Piece>();
      }

      return new HashSet<Piece>(threateningEnemies[square]);
    }
    
    
    public HashSet<Piece> GetThreatenedEnemies(Square square){
      if(!threatenedEnemies.ContainsKey(square)) {
        return new HashSet<Piece>();
      }

      return new HashSet<Piece>(threatenedEnemies[square]);
    }
    
  
  }
}