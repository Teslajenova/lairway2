using System;

namespace Lairway{
  public class Square{
  
    private Point coordinates;
  
  
    public Square(Point coordinates){
      this.coordinates = coordinates;
    }
    
    
    public Square(int x, int y) : this(new Point(x, y)){
    }
    
    
    public Point Coordinates{
      get{
        return new Point(coordinates.X, coordinates.Y);
      }
    }
    
    [Obsolete("Use .Coordonates instead")]
    public Point GetCoordinates(){
      return new Point(coordinates.X, coordinates.Y);
    }
    
    
    public bool Equals(Square other){
      return this.Coordinates.Equals(other.Coordinates);
    }
  
  }
}