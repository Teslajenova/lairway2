namespace Lairway{ //todo: fix the namespace
  public class Piece{
    
    private Square position;
    private Stance stance;
    private Band band;
    
    private int hp; // todo: rename to stamina or vitality? could also be depleted by pushing the piece too hard
    private int move;
    
    
    public Piece(Square position, Stance stance, Band band, int hp, int move){
      this.position = position;
      this.stance = stance;
      this.band = band;
      this.hp = hp;
      this.move = move;
    }
    
    
    public bool MoveTo(Square destination){
      if(destination == null){
        //todo: log error
        return false;
      }
      
      position = destination;
      return true;
    }
    
    
    public Square Position{
      get{
        return position;
      }
    }
    
    
    public Stance Stance{
      get{
        return stance;
      }
    }
    
    
    public Band Band{
      get{
        return band;
      }
    }
    
    public int HP{
      get{
        return hp;
      }
    }
    
    
    public int Move{
      get{
        return move;
      }
    }
    
    
  }
}