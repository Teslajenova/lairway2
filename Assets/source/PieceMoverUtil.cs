using System.Collections.Generic;
using System.Linq;

namespace Lairway{
  public class PieceMoverUtil{
    
    // todo: most of these could be optimised by caching move ranges with pieces and updating caches only when moving
    
    public static HashSet<Square> GetMoveRange(Piece piece, BattleStage battleStage, bool diagonal){
      HashSet<Square> movableSquares = new HashSet<Square>();
      
      Square currentSquare = piece.Position;
      
      if(!battleStage.Board.Squares.Contains(currentSquare)){
        //todo: log error
        return movableSquares;
      }
      
      HashSet<Square> bases = new HashSet<Square>();
      bases.Add(currentSquare);
      
      HashSet<Square> found = new HashSet<Square>();
      
      for(int i = 0; i < piece.Move; i++){
        // find new squares connected to bases
        foreach(Square s in bases){
          if(diagonal){
            found.UnionWith(battleStage.Board.GetSurroundingSquares(s));
          }else{
            found.UnionWith(battleStage.Board.GetOrthogonalSquares(s));
          }
        }
        
        // add found squares to movableSquares
        movableSquares.UnionWith(found);
        
        // reset bases & found
        bases.Clear();
        bases.UnionWith(found);
        found.Clear();
      }
      
      // can't move to where someone is

      foreach(Square square in battleStage.Board.Squares) {
        if(battleStage.GetOccupantOf(square) != null) {
          movableSquares.Remove(square);
        }
      }

      //movableSquares.Remove(currentSquare);
      // todo: maybe remove other occupied squares here too?
      
      return movableSquares;
    }
    
    // todo: these might be better to move to another util
    //----------------------------------------------------
    
    public static List<Square> GetDangerRange(Piece piece, BattleStage battleStage, HashSet<Piece> opponents, bool diagonal){
      // get full move range
      HashSet<Square> moveRange = GetMoveRange(piece, battleStage, diagonal);
        
      // Get opponents' control ranges
      HashSet<Square> controlled = new HashSet<Square>();
      
      foreach(Piece opponent in opponents){
        controlled.UnionWith(PieceControlUtil.GetControlledSquares(opponent, battleStage));
      }
      
      // return their overlap
      return moveRange.Where(square => controlled.Contains(square)).ToList();
    }
    
    
    // negative danger range
    public static List<Square> GetSafeRange(Piece piece, BattleStage battleStage, HashSet<Piece> opponents, bool diagonal){
      // get full move range
      HashSet<Square> moveRange = GetMoveRange(piece, battleStage, diagonal);
      
      // get danger range
      List<Square> dangerRange = GetDangerRange(piece, battleStage, opponents, diagonal);
      
      return moveRange.Where(square => !dangerRange.Contains(square)).ToList();
    }
    
    /*
    public static HashSet<Square> GetOpportunityRange(Piece piece, Board board, HashSet<Piece> opponents, bool diagonal){
      // get full move range
      HashSet<Square> moveRange = GetMoveRange(piece, board, diagonal);
      
      // get 
      
    }
    */
    
  }
}
