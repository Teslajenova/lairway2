using System;
using System.Linq;
using System.Collections.Generic;

namespace Lairway{
  public class BattleStage{
    
    private Board board;
    private HashSet<Piece> pieces = new HashSet<Piece>();
    //private Dictionary<Piece, Band> pieces = new Dictionary<Piece, Band>();
    
    
    public BattleStage(){
      // Example setup
      board = BoardFactory.MakeRectangleBoard(13, 9);
      
      SetUpAlliedPieces();
      SetUpEnemyPieces();
    }
    
    
    private void SetUpAlliedPieces(){
      pieces.Add(new Piece(board.GetSquare(0, 0), Stance.Stoic, Band.Ally, 3, 4));
      pieces.Add(new Piece(board.GetSquare(1, 3), Stance.Deft, Band.Ally, 3, 3));
      pieces.Add(new Piece(board.GetSquare(0, 6), Stance.Wily, Band.Ally, 3, 2));
    }
    
    
    private void SetUpEnemyPieces(){
      pieces.Add(new Piece(board.GetSquare(7, 0), Stance.Wily, Band.Enemy, 3, 3));
      pieces.Add(new Piece(board.GetSquare(4, 4), Stance.Stoic, Band.Enemy, 3, 4));
      pieces.Add(new Piece(board.GetSquare(6, 6), Stance.Deft, Band.Enemy, 3, 2));
    }
    
    
    public Board Board{
      get{
        return board;
      }
    }
    
    [Obsolete("Use .Board instead.")]
    public Board GetBoard(){
      return board;
    }
    
    
    public Piece GetOccupantOf(Square square){
      foreach(Piece piece in pieces){
        if(piece.Position.Equals(square)){
          return piece;
        }
      }
      
      //Todo: log event
      return null;
    }
    
    
    public HashSet<Piece> Pieces{
      get{
        return new HashSet<Piece>(pieces);
      }
    }
    
    
    public List<Piece> GetPieces(Band band){
      return pieces.Where(piece => piece.Band == band).ToList();
      
      /*
      HashSet<Piece> bandPieces = new HashSet<Piece>();
      
      foreach(KeyValuePair<Piece, Band> pair in pieces){
        if(pair.Value == band){
          bandPieces.Add(pair.Key);
        }
      }
      
      return bandPieces;
      */
    }
    
    
  }
}