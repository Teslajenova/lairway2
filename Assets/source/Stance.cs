using System.Collections.Generic;

namespace Lairway{
  public enum Stance{
    Deft,
    Stoic,
    Wily,
    
    Down
  }
  
  
  public static class StanceExtension{
  
    public static HashSet<Square> GetControlledSquares(this Stance stance, Square square, BattleStage battleStage, Piece ignoredPiece){
      if(!battleStage.Board.Squares.Contains(square)){
        // todo: log error
        return new HashSet<Square>();
      }
    
      if(stance == Stance.Deft){
        return DeftControl(square, battleStage.Board);
      }else if(stance == Stance.Stoic){
        return StoicControl(square, battleStage, ignoredPiece);
      }else if(stance == Stance. Wily){
        return WilyControl(square, battleStage, ignoredPiece);
      }else{
        return DownControl(square, battleStage, ignoredPiece);
      }
    }
    
    
    private static HashSet<Square> DeftControl(Square square, Board board){
      HashSet<Square> controlled = new HashSet<Square>();
      
      // todo: probably needs to distinguish between pieces of different bands
      
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X -2, square.Coordinates.Y -1));
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X -2, square.Coordinates.Y +1));
        
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X +2, square.Coordinates.Y -1));
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X +2, square.Coordinates.Y +1));
        
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X -1, square.Coordinates.Y -2));
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X +1, square.Coordinates.Y -2));
        
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X -1, square.Coordinates.Y +2));
      CollectionUtil<Square>.AddIfNotNull(controlled, 
        board.GetSquare(square.Coordinates.X +1, square.Coordinates.Y +2));
        
      return controlled;
    }
    
    
    private static HashSet<Square> StoicControl(Square square, BattleStage battleStage, Piece ignoredPiece){
      HashSet<Square> controlled = new HashSet<Square>();
      
      Square north = battleStage.Board.GetSquare(square.Coordinates.X, square.Coordinates.Y -1);
      
      Square south = battleStage.Board.GetSquare(square.Coordinates.X, square.Coordinates.Y +1);
      
      Square west = battleStage.Board.GetSquare(square.Coordinates.X -1, square.Coordinates.Y);
      
      Square east = battleStage.Board.GetSquare(square.Coordinates.X +1, square.Coordinates.Y);
      
      AddLine(controlled, battleStage, north, 0, -1, ignoredPiece);
      
      AddLine(controlled, battleStage, south, 0, +1, ignoredPiece);
      
      AddLine(controlled, battleStage, west, -1, 0, ignoredPiece);
      
      AddLine(controlled, battleStage, east, +1, 0, ignoredPiece);
      
      return controlled;
    }
    
    
    private static HashSet<Square> WilyControl(Square square, BattleStage battleStage, Piece ignoredPiece){
      HashSet<Square> controlled = new HashSet<Square>();
      
      Square nw = battleStage.Board.GetSquare(square.Coordinates.X -1, square.Coordinates.Y -1);
      
      Square ne = battleStage.Board.GetSquare(square.Coordinates.X +1, square.Coordinates.Y -1);
      
      Square sw = battleStage.Board.GetSquare(square.Coordinates.X -1, square.Coordinates.Y +1);
      
      Square se = battleStage.Board.GetSquare(square.Coordinates.X +1, square.Coordinates.Y +1);
      
      AddLine(controlled, battleStage, nw, -1, -1, ignoredPiece);
      
      AddLine(controlled, battleStage, ne, 1, -1, ignoredPiece);
      
      AddLine(controlled, battleStage, sw, -1, 1, ignoredPiece);
      
      AddLine(controlled, battleStage, se, 1, 1, ignoredPiece);
      
      return controlled;
    }
    
    
    private static HashSet<Square> DownControl(Square square, BattleStage battleStage, Piece ignoredPiece){
      HashSet<Square> controlled = new HashSet<Square>();
      
      controlled.Add(battleStage.Board.GetSquare(square.Coordinates.X -1, square.Coordinates.Y -1));
      
      controlled.Add(battleStage.Board.GetSquare(square.Coordinates.X +1, square.Coordinates.Y -1));
      
      controlled.Add(battleStage.Board.GetSquare(square.Coordinates.X -1, square.Coordinates.Y +1));
      
      controlled.Add(battleStage.Board.GetSquare(square.Coordinates.X +1, square.Coordinates.Y +1));
      
      return controlled;
    }
    
    
    private static void AddLine(HashSet<Square> controlled, BattleStage battleStage, Square next, int modX, int modY, Piece ignoredPiece){
    
      // todo: probably needs to distinguish between pieces of different bands
      
      while(next != null){
        controlled.Add(next);
        
        Piece occupant = battleStage.GetOccupantOf(next);
        
        if(occupant == null || occupant == ignoredPiece){
          next = battleStage.Board.GetSquare(next.Coordinates.X + modX, next.Coordinates.Y + modY);
        }else{
          next = null;
        }
      }
    }


  }
}
