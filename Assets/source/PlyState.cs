using System.Collections.Generic;

namespace Lairway{
  public class PlyState{
    
    private HashSet<Arrow> arrows;
    private HashSet<ThreatState> threats = new HashSet<ThreatState>();
    
    
    public PlyState(BattleStage battleStage){
      arrows = ArrowUtil.GetArrows(battleStage);
      
      MakeThreatsFromArrows();
    }
    
    
    private void MakeThreatsFromArrows(){
      foreach(Arrow arrow in arrows){
        Piece target = arrow.Target;
        
        ThreatState threatState = GetStoredThreatState(target);
        threatState.AddArrow(arrow);
      }
    }
    
    
    private ThreatState GetStoredThreatState(Piece target){
      foreach(ThreatState threatState in threats){
        if(threatState.Target == target){
          return threatState;
        }
      }
      
      ThreatState newThreatState = new ThreatState(target); 
      threats.Add(newThreatState);
      return newThreatState;
    }
     
     
     public ThreatLevel GetThreatLevelFor(Piece target){
       foreach(ThreatState threatState in threats){
         if(threatState.Target == target){
           return threatState.GetThreat();
         }
       }
       
       return ThreatLevel.Normal;
     }
     
     
     public HashSet<Arrow> Arrows{
       get{
         return new HashSet<Arrow>(arrows);
       }
     }
     
  }
}
