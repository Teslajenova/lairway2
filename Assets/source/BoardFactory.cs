using System.Collections.Generic;

namespace Lairway{
  public class BoardFactory{
  
    public static Board MakeSquareBoard(int size){
      return MakeRectangleBoard(size, size);
    }
    
    
    public static Board MakeRectangleBoard(int width, int height){
      HashSet<Square> squares = new HashSet<Square>();
    
      for(int x = 0; x < width; x++){
        for(int y = 0; y < height; y++){
          squares.Add(new Square(x, y));
        }
      }
      
      return new Board(squares);
    }
    
  }
}