using Lairway;
using System;
using System.Collections.Generic;

namespace Lairway{ // todo: fix the namespace 
  public class Board{
    
    private HashSet<Square> squares;
    
    
    public Board(HashSet<Square> squares){ 
      this.squares = squares;
    }
    
    
    public HashSet<Square> GetSurroundingSquares(Square midSquare){
      HashSet<Square> surroundingSquares = GetOrthogonalSquares(midSquare);
      surroundingSquares.UnionWith(GetDiagonalSquares(midSquare));
      return surroundingSquares;
    }
    
    
    public HashSet<Square> GetOrthogonalSquares(Square midSquare){
      HashSet<Square> orthogonals = new HashSet<Square>();
      
      if (midSquare == null){
        //todo: add log error
        return orthogonals;
      }
      
      CollectionUtil<Square>.AddIfNotNull(orthogonals, 
        GetSquare(midSquare.Coordinates.X -1, midSquare.Coordinates.Y));
      CollectionUtil<Square>.AddIfNotNull(orthogonals, 
        GetSquare(midSquare.Coordinates.X +1, midSquare.Coordinates.Y));
      CollectionUtil<Square>.AddIfNotNull(orthogonals, 
        GetSquare(midSquare.Coordinates.X, midSquare.Coordinates.Y -1));
      CollectionUtil<Square>.AddIfNotNull(orthogonals, 
        GetSquare(midSquare.Coordinates.X, midSquare.Coordinates.Y +1));
      
      return orthogonals;
    }
    
    
    public HashSet<Square> GetDiagonalSquares(Square midSquare){
      HashSet<Square> diagonals = new HashSet<Square>();
      
      if (midSquare == null){
        //todo: add log error
        return diagonals;
      }
      
      CollectionUtil<Square>.AddIfNotNull(diagonals, GetSquare(midSquare.Coordinates.X -1, midSquare.Coordinates.Y -1));

      CollectionUtil<Square>.AddIfNotNull(diagonals, GetSquare(midSquare.Coordinates.X -1, midSquare.Coordinates.Y +1));

      CollectionUtil<Square>.AddIfNotNull(diagonals, GetSquare(midSquare.Coordinates.X +1, midSquare.Coordinates.Y -1));

      CollectionUtil<Square>.AddIfNotNull(diagonals, GetSquare(midSquare.Coordinates.X +1, midSquare.Coordinates.Y +1));
      
      return diagonals;
    }
    
    
    public Square GetSquare(int x, int y){
      return GetSquare(new Point(x, y));
    }
    
    
    public Square GetSquare(Point coordinates){
      Square desiredSquare = null;
      
      foreach(Square square in squares){
        if(square.Coordinates.Equals(coordinates)){
          desiredSquare = square;
        }
      }
      
      if(desiredSquare == null){
        //todo: add log error for no square found
      }
      
      return desiredSquare;
    }
    
    
    public HashSet<Square> Squares{
      get{
        return new HashSet<Square>(squares);
      }
    }
    
    [Obsolete("Use .Squares instead.")]
    public HashSet<Square> GetSquares(){
      return new HashSet<Square>(squares);
    }
    
    
  }
}