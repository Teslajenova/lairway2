namespace Lairway{
  public enum ThreatLevel{
    Normal,
    Defenceless,
    WinningChain,
    LosingChain
  }
}