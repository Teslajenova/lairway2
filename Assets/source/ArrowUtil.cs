using System.Collections.Generic;


namespace Lairway{
  public class ArrowUtil{
    
    public static HashSet<Arrow> GetArrows(BattleStage battleStage){
      HashSet<Arrow> arrows = new HashSet<Arrow>();
      
      foreach(Piece piece in battleStage.Pieces){
        HashSet<Piece> all = new HashSet<Piece>();
      
        all.UnionWith(PieceCoverUtil.GetCoveredPieces(piece.Position, piece.Stance, piece.Band, battleStage));
        all.UnionWith(PieceCoverUtil.GetCoveredPieces(piece.Position, piece.Stance, piece.Band.Opposite(), battleStage));
        
        foreach(Piece otherPiece in all){
          arrows.Add(new Arrow(piece, otherPiece));
        }
      }
      
      return arrows;
    }
    
  }
}
