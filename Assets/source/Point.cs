namespace Lairway{
  public class Point{
    
    private int x;
    private int y;
    
    
    public Point(): this(0, 0){
    }
    
    
    public Point(int x, int y){
      this.x = x;
      this.y = y;
    }
    
    
    public int X{
      get{
        return x;
      }
      set{
        x = value;
      }
    }
    
    
    public int Y{
      get{
        return y;
      }
      set{
        y = value;
      }
    }
    
    
    public bool Equals(Point other){
      if(this.x == other.X && this.y == other.Y){
        return true;
      }
      
      return false;
    }
    
  }
}