using System.Collections.Generic;

namespace Lairway{
  public class PieceCoverUtil{
  
    
    public static HashSet<Piece> GetCoveringPieces(Square square, Band band, BattleStage battleStage){
      return GetCoveringPieces(square, band, battleStage, null);
    }
    
    
    public static HashSet<Piece> GetCoveringPieces(Square square, Band band, BattleStage battleStage, Piece ignoredPiece){
      return GetCoveringPieces(square, new HashSet<Piece>(battleStage.GetPieces(band)), battleStage, ignoredPiece);
    }
    
    
    public static HashSet<Piece> GetCoveringPieces(Square square, HashSet<Piece> pieces, BattleStage battleStage){
      return GetCoveringPieces(square, pieces, battleStage, null);
    }
    
  
    public static HashSet<Piece> GetCoveringPieces(Square square, HashSet<Piece> pieces, BattleStage battleStage, Piece ignoredPiece){
      if(ignoredPiece != null) {
        pieces.Remove(ignoredPiece);
      }

      HashSet<Piece> covers = new HashSet<Piece>();
      
      if(!battleStage.Board.Squares.Contains(square)){
        // todo: log error
        return covers;
      }
      
      foreach(Piece piece in pieces){
        if(PieceControlUtil.GetControlledSquares(piece, battleStage, ignoredPiece).Contains(square)){
          covers.Add(piece);
        }
      }
      
      return covers;
    }
    
    
    public static HashSet<Piece> GetCoveredPieces(Square square, Stance stance, Band band, BattleStage battleStage){
      return GetCoveredPieces(square, stance, band, battleStage, null);
    }
    
    
    public static HashSet<Piece> GetCoveredPieces(Square square, Stance stance, Band band, BattleStage battleStage, Piece ignoredPiece){
      return GetCoveredPieces(square, stance, new HashSet<Piece>(battleStage.GetPieces(band)), battleStage, ignoredPiece);
    }
    
    
    public static HashSet<Piece> GetCoveredPieces(Square square, Stance stance, HashSet<Piece> pieces, BattleStage battleStage){
      return GetCoveredPieces(square, stance, pieces, battleStage, null);
    }
    
    
    public static HashSet<Piece> GetCoveredPieces(Square square, Stance stance, HashSet<Piece> pieces, BattleStage battleStage, Piece ignoredPiece){
      if(ignoredPiece != null) {
        pieces.Remove(ignoredPiece);
      }

      HashSet<Piece> covered = new HashSet<Piece>();
      
      if(!battleStage.Board.Squares.Contains(square)){
        // todo: log error
        return covered;
      }
      
      HashSet<Square> controlled = stance.GetControlledSquares(square, battleStage, ignoredPiece);
      
      foreach(Piece piece in pieces){
        if(controlled.Contains(piece.Position)){
          covered.Add(piece);
        }
      }
      
      return covered;
    }
    
  
  }
}
