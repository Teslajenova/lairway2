namespace Lairway{
  public enum Band{
    Ally,
    Enemy
  }

  public static class BandExtension{
    
    public static Band Opposite(this Band band){  
      if(band == Band.Ally){
        return Band.Enemy;
      }else{
        return Band.Ally;
      }
    }
    
  }
}