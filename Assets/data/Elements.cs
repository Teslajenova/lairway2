﻿using Lairway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class Elements : MonoBehaviour {

  public Transform threatArrow;


  private Transform pointer;

  private Dictionary<Square, Transform> squareTiles = new Dictionary<Square, Transform>();

  private Dictionary<Square, Transform> threatMarkers = new Dictionary<Square, Transform>();
  private Dictionary<Square, Transform> aidMarkers = new Dictionary<Square, Transform>();
  private Dictionary<Square, Transform> coverMarkers = new Dictionary<Square, Transform>();
  private Dictionary<Square, Transform> opportunityMarkers = new Dictionary<Square, Transform>();

  private Dictionary<Piece, Transform> pieceQuads = new Dictionary<Piece, Transform>();


  public static Transform GetPointer() {
    if(INSTANCE == null) {
      return null;

      //todo: log error
    }

    return INSTANCE.pointer;
  }


  public static bool SetPointer(Transform pointer) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.pointer = pointer;
    return true;
  }


  public static Transform GetThreatArrow() {
    if(INSTANCE == null) {
      return null;

      //todo: log error
    }

    return INSTANCE.threatArrow;
  }


  public static bool SetThreatArrow(Transform threatArrow) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.threatArrow = threatArrow;
    return true;
  }


  public static bool PutSquareTile(Square square, Transform tile) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.squareTiles.Add(square, tile);
    return true;
  }


  public static Transform GetSquareTile(Square square) {
    if(INSTANCE == null) {
      return null;

      // todo: log error
    }

    return INSTANCE.squareTiles[square];
  }


  public static bool PutThreatMarker(Square square, Transform marker) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.threatMarkers.Add(square, marker);
    return true;
  }


  public static Transform GetThreatMarker(Square square) {
    if(INSTANCE == null) {
      return null;

      // todo: log error
    }

    return INSTANCE.threatMarkers[square];
  }


  public static bool PutAidMarker(Square square, Transform marker) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.aidMarkers.Add(square, marker);
    return true;
  }


  public static Transform GetAidMarker(Square square) {
    if(INSTANCE == null) {
      return null;

      // todo: log error
    }

    return INSTANCE.aidMarkers[square];
  }


  public static bool PutCoverMarker(Square square, Transform marker) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.coverMarkers.Add(square, marker);
    return true;
  }


  public static Transform GetCoverMarker(Square square) {
    if(INSTANCE == null) {
      return null;

      // todo: log error
    }

    return INSTANCE.coverMarkers[square];
  }


  public static bool PutOpportunityMarker(Square square, Transform marker) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.opportunityMarkers.Add(square, marker);
    return true;
  }


  public static Transform GetOpportunityMarker(Square square) {
    if(INSTANCE == null) {
      return null;

      // todo: log error
    }

    return INSTANCE.opportunityMarkers[square];
  }


  public static bool PutPieceQuad(Piece piece, Transform quad) {
    if(INSTANCE == null) {
      return false;

      // todo: log error
    }

    INSTANCE.pieceQuads.Add(piece, quad);
    return true;
  }


  public static Transform GetPieceQuad(Piece piece) {
    if(INSTANCE == null) {
      return null;

      // todo: log error
    }

    return INSTANCE.pieceQuads[piece];
  }




  // Persistence members
  private static Elements INSTANCE;

  void Awake() {
    if(INSTANCE == null) {
      INSTANCE = this;
    } else if(INSTANCE != this) {
      Destroy(gameObject);
    }

    DontDestroyOnLoad(gameObject);
  }

}