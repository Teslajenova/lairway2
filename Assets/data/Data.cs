using Lairway;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour {
  
  private BattleStage battleStage;

  private Square selectedSquare;
  private PieceScope pieceScope;
  

  public BattleStage BattleStage{
    get{
      return battleStage;
    }
    set{
      battleStage = value;
    }
  }


  public Square SelectedSquare {
    get {
      return selectedSquare;
    }
    set {
      selectedSquare = value;
    }
  }


  public PieceScope PieceScope {
    get {
      return pieceScope;
    }
    set {
      pieceScope = value;
    }
  }


  // shorthand method, not strictly necessary
  public static BattleStage GetBattleStage(){
    if(INSTANCE == null){
      return null;

      //todo: log error
    }
    
    return INSTANCE.BattleStage;
  }
  
  
  // Persistence members
  public static Data INSTANCE;
  
  void Awake(){
    if(INSTANCE == null){
      INSTANCE = this;
    } else if(INSTANCE != this){
      Destroy(gameObject);
    }

    DontDestroyOnLoad(gameObject);
  }

  
}