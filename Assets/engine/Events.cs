
using System;
using System.Collections.Generic;

namespace Lairway.Engine{
  public class Events {
    
    public static readonly String PIECE_SCOPE_REVEALED = "a piece scope got revealed";
    public static readonly String PIECE_SCOPE_RETRACTED = "a piece scope got retracted";
    public static readonly String PIECE_MOVED = "a piece got moved";

    private static List<String> eventCache = new List<String>();
    
    
    public static void Place(String e){
      eventCache.Add(e);
    }
    
    
    public static bool Has(String e){
      return eventCache.Remove(e);
    }


    public static void Clear(){
      eventCache.Clear();
    }
    
  }
}