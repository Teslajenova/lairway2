namespace Lairway{//todo: maybe change this
  public class Constants{

    public static readonly float CAMERA_Z = -10.0f;

    public static readonly float BOARD_Z = 10.0f;

    public static readonly float PIECE_Z = 8.0f;

    public static readonly float POINTER_Z = -5.0f;

    public static readonly float THREAT_MARKER_Z = -4.0f;
    
    public static readonly float AID_MARKER_Z = -3.0f;
    
    public static readonly float COVER_MARKER_Z = -2.0f;
    
    public static readonly float OPPORTUNITY_MARKER_Z = -1.0f;
    
    
  }
}