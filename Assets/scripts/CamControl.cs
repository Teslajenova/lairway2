using Lairway;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CamControl : MonoBehaviour {
  
  private bool cameraIsSet = false;

  public Camera mainCamera;
  
  
  // Use this for initialization
  void Start(){
    
  }
  
  
  // Update is called once per frame
  void Update(){
    if(!cameraIsSet) {
      CameraSetup();
    }
  }
  

  private void CameraSetup() {
    HashSet<Square> boardSquares = Data.INSTANCE.BattleStage.Board.Squares;
    
    Square firstSquare = boardSquares.ToList()[0];
  
    float tileMinX = firstSquare.Coordinates.X;
    float tileMaxX = tileMinX;

    float tileMinY = firstSquare.Coordinates.Y;
    float tileMaxY = tileMinY;

    foreach(Square square in boardSquares) {
      if(square.Coordinates.X < tileMinX) {
        tileMinX = square.Coordinates.X;
      }else if(square.Coordinates.X > tileMaxX) {
        tileMaxX = square.Coordinates.X;
      }

      if(square.Coordinates.Y < tileMinY) {
        tileMinY = square.Coordinates.Y;
      } else if(square.Coordinates.Y > tileMaxY) {
        tileMaxY = square.Coordinates.Y;
      }
    }

    float midX = tileMinX + ((tileMaxX - tileMinX) / 2);
    float midY = tileMinY + ((tileMaxY - tileMinY) / 2);

    mainCamera.gameObject.transform.position = new Vector3(midX, midY, Constants.CAMERA_Z);

    cameraIsSet = true;
  }

}