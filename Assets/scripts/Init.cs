﻿using Lairway;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Init : MonoBehaviour {

  public Transform pointer;

  public Transform pieceQuad;

  public Transform boardTile;

  public Transform threatenedMarker;
  public Transform aidMarker;
  public Transform coveredMarker;
  public Transform opportunityMarker;

  // Use this for initialization
  void Start () {
    CreateBattleStage();
    CreateStageBoard();
    CreateStagePieces();
    CreatePointer();

  }
	

  // Update is called once per frame
  void Update () {
    
  }


  private void CreateBattleStage() {
    Data.INSTANCE.BattleStage = new BattleStage();
  }


  private void CreateStageBoard() {
    HashSet<Square> squares = Data.INSTANCE.BattleStage.Board.Squares;

    foreach(Square square in squares) {
      // board bg
      Transform localTile = Instantiate(boardTile, new Vector3(square.Coordinates.X, square.Coordinates.Y, Constants.BOARD_Z), Quaternion.Euler(0, 0, 0));
      Elements.PutSquareTile(square, localTile);

      // threat markers
      Transform localThreatenedMarker = Instantiate(threatenedMarker, new Vector3(square.Coordinates.X, square.Coordinates.Y, Constants.THREAT_MARKER_Z), Quaternion.Euler(0, 0, 0));
      localThreatenedMarker.gameObject.SetActive(false);
      Elements.PutThreatMarker(square, localThreatenedMarker);

      // aid markers
      Transform localAidMarker = Instantiate(aidMarker, new Vector3(square.Coordinates.X, square.Coordinates.Y, Constants.AID_MARKER_Z), Quaternion.Euler(0, 0, 0));
      localAidMarker.gameObject.SetActive(false);
      Elements.PutAidMarker(square, localAidMarker);

      // cover markers
      Transform localCoverMarker = Instantiate(coveredMarker, new Vector3(square.Coordinates.X, square.Coordinates.Y, Constants.COVER_MARKER_Z), Quaternion.Euler(0, 0, 0));
      localCoverMarker.gameObject.SetActive(false);
      Elements.PutCoverMarker(square, localCoverMarker);

      // opportunity markers
      Transform localOpportunityMarker = Instantiate(opportunityMarker, new Vector3(square.Coordinates.X, square.Coordinates.Y, Constants.OPPORTUNITY_MARKER_Z), Quaternion.Euler(0, 0, 0));
      localOpportunityMarker.gameObject.SetActive(false);
      Elements.PutOpportunityMarker(square, localOpportunityMarker);
    }
  }


  private void CreateStagePieces() {
    HashSet<Piece> pieces = Data.INSTANCE.BattleStage.Pieces;

    foreach(Piece piece in pieces) {
      Square position = piece.Position;

      Transform localQuad = Instantiate(pieceQuad, new Vector3(position.Coordinates.X, position.Coordinates.Y, Constants.PIECE_Z), Quaternion.Euler(0, 0, 0));
      Elements.PutPieceQuad(piece, localQuad);
    }

  }


  private void CreatePointer() {
    Piece firstPiece = Data.INSTANCE.BattleStage.Pieces.ToList()[0];
    Square square = firstPiece.Position;

    Elements.SetPointer(Instantiate(pointer, new Vector3(square.Coordinates.X, square.Coordinates.Y, Constants.POINTER_Z), Quaternion.Euler(0, 0, 0)));
  }



}
