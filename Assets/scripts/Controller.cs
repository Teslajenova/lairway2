﻿using Lairway;
using Lairway.Engine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Controller : MonoBehaviour {

  // Use this for initialization
  void Start() {
  }
  
  
  // Update is called once per frame
  void Update() {
    // selection setup
    if(Data.INSTANCE.SelectedSquare == null) {
      Data.INSTANCE.SelectedSquare = Data.GetBattleStage().Board.Squares.ToList()[0];
    }

    UpdatePointer();

    Actions();
  }


  private void UpdatePointer() {
    Transform pointer = Elements.GetPointer();

    if(pointer == null) {
      //todo: log error
      return;
    }

    Square nextSquare = null;

    if(Input.GetKeyDown(KeyCode.RightArrow)){
      Square ss = Data.INSTANCE.SelectedSquare;
      nextSquare = Data.GetBattleStage().Board.GetSquare(ss.Coordinates.X +1, ss.Coordinates.Y);
    }

    if(Input.GetKeyDown(KeyCode.LeftArrow)){
      Square ss = Data.INSTANCE.SelectedSquare;
      nextSquare = Data.GetBattleStage().Board.GetSquare(ss.Coordinates.X -1, ss.Coordinates.Y);
    }

    if(Input.GetKeyDown(KeyCode.DownArrow)) {
      Square ss = Data.INSTANCE.SelectedSquare;
      nextSquare = Data.GetBattleStage().Board.GetSquare(ss.Coordinates.X, ss.Coordinates.Y -1);
    }

    if(Input.GetKeyDown(KeyCode.UpArrow)) {
      Square ss = Data.INSTANCE.SelectedSquare;
      nextSquare = Data.GetBattleStage().Board.GetSquare(ss.Coordinates.X, ss.Coordinates.Y +1);
    }

    // update selected square
    if(nextSquare != null) {
      Data.INSTANCE.SelectedSquare = nextSquare;
      
      //update pointer position
      pointer.position = new Vector3(nextSquare.Coordinates.X, nextSquare.Coordinates.Y, Constants.POINTER_Z);
    }
  }


  private void Actions() {

    //return
    if(Input.GetKeyDown(KeyCode.Return)) {
      OK();
    }

    //shift
    if(Input.GetKeyDown(KeyCode.RightShift)) {
      Back();
    }

  }


  private void OK(){
    Piece piece = Data.INSTANCE.BattleStage.GetOccupantOf(Data.INSTANCE.SelectedSquare);

    // reveal piece scope
    if(piece != null && Data.INSTANCE.PieceScope == null) {
      Data.INSTANCE.PieceScope = new PieceScope(piece, Data.INSTANCE.BattleStage, true);
      Events.Place(Events.PIECE_SCOPE_REVEALED);
      return;
    }

    // move piece
    if(Data.INSTANCE.PieceScope != null && Data.INSTANCE.PieceScope.MoveRange.Contains(Data.INSTANCE.SelectedSquare)) {
      Data.INSTANCE.PieceScope.Piece.MoveTo(Data.INSTANCE.SelectedSquare);
      Events.Place(Events.PIECE_MOVED);
      return;
    }
  }


  private void Back() {

    // retract piece scope
    if(Data.INSTANCE.PieceScope != null) {
      Data.INSTANCE.PieceScope = null;
      Events.Place(Events.PIECE_SCOPE_RETRACTED);
      return;
    }

  }

}
