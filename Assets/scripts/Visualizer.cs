using Lairway;
using Lairway.Engine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Visualizer : MonoBehaviour {
  // this class is here to lift the burden of creating on-screen changes and animations from other classes with other concerns
  // this class listens to events raised by those other classes via the Events class
  
  // Use this for initialization
  void Start() {
    
  }
  
  
  // Update is called once per frame
  void Update() {
    if(Events.Has(Events.PIECE_SCOPE_REVEALED)) {
      PieceScopeRevealed();
    }

    if(Events.Has(Events.PIECE_SCOPE_RETRACTED)) {
      PieceScopeRetracted();
    }

    if(Events.Has(Events.PIECE_MOVED)) {
      PieceMoved();
    }

  }


  private void PieceScopeRevealed() {
    RemoveTint();
    TintRange();
    HideMarkers();
    RevealMarkers();
  }


  private void PieceScopeRetracted() {
    RemoveTint();
    HideMarkers();
  }


  private void PieceMoved() {
    Piece movedPiece = Data.INSTANCE.PieceScope.Piece; 
    // todo: it's not clear why this is the recently moved piece. do something about that
    
    // reposition the piece quad
    Transform quad = Elements.GetPieceQuad(movedPiece);
    quad.position = new Vector3(movedPiece.Position.Coordinates.X, movedPiece.Position.Coordinates.Y, Constants.PIECE_Z);

    // retract the piece scope, as it's invalid now
    // todo: maybe this shouldn't happen here
    Data.INSTANCE.PieceScope = null;
    PieceScopeRetracted();
  }


  private void TintRange() {
    foreach(Square square in Data.INSTANCE.BattleStage.Board.Squares) {
      if(Data.INSTANCE.PieceScope.MoveRange.Contains(square)) {
        Elements.GetSquareTile(square).GetComponent<Renderer>().material.SetColor("_Color", Color.green);
      }
    }
  }


  private void RemoveTint() {
    foreach(Square square in Data.INSTANCE.BattleStage.Board.Squares) {
      Elements.GetSquareTile(square).GetComponent<Renderer>().material.SetColor("_Color", Color.white);
    }
  }


  private void HideMarkers() {
    foreach(Square square in Data.INSTANCE.BattleStage.Board.Squares) {
      Elements.GetThreatMarker(square).gameObject.SetActive(false);

      Elements.GetAidMarker(square).gameObject.SetActive(false);

      Elements.GetCoverMarker(square).gameObject.SetActive(false);

      Elements.GetOpportunityMarker(square).gameObject.SetActive(false);
    }
  }


  private void RevealMarkers() {
    foreach(Square square in Data.INSTANCE.BattleStage.Board.Squares) {
      if(Data.INSTANCE.PieceScope.GetThreateningEnemies(square).Count > 0) {
        Elements.GetThreatMarker(square).gameObject.SetActive(true);
      }

      if(Data.INSTANCE.PieceScope.GetCoveredAllies(square).Count > 0) {
        Elements.GetAidMarker(square).gameObject.SetActive(true);
      }

      if(Data.INSTANCE.PieceScope.GetCoveringAllies(square).Count > 0) {
        Elements.GetCoverMarker(square).gameObject.SetActive(true);
      }

      if(Data.INSTANCE.PieceScope.GetThreatenedEnemies(square).Count > 0) {
        Elements.GetOpportunityMarker(square).gameObject.SetActive(true);
      }
    }
  }

}